package com.example.demo.cache;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.ClusterServersConfig;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * Redisson配置类
 * Created on 2021/05/06
 */
@Configuration
public class RedissonConfig {
    @Value("#{'${spring.redis.host}' + ':' + '${spring.redis.port}'}")
    private String singleRedisHost;

    @Value("${spring.redis.cluster.nodes}")
    private List<String> cluster;

    @Value("${spring.redis.password}")
    private String clusterPassword;

    @Bean(name = "SingleRedissonClient")
    public RedissonClient SingleRedissonClient() {
        Config config = new Config();
        config.useSingleServer().setAddress("redis://" + singleRedisHost);
        return Redisson.create(config);
    }

    @Bean(name = "ClusterRedissonClient")
    public RedissonClient ClusterRedissonClient() {
        List<String> clusterNodes = new ArrayList<>();
        for (String node : cluster) {
            clusterNodes.add("redis://" + node);
        }
        Config config = new Config();
        // 添加集群地址
        ClusterServersConfig clusterServersConfig = config.useClusterServers().addNodeAddress(clusterNodes.toArray(new String[0]));
        // 设置密码
        clusterServersConfig.setPassword(clusterPassword);
        return Redisson.create(config);
    }
}
