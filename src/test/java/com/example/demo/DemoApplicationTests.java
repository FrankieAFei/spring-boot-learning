package com.example.demo;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.Objects;

@SpringBootTest
class DemoApplicationTests {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Test
    void contextLoads() {
    }

    @Test
    void testRedis() {
        // 写入字符串
        stringRedisTemplate.opsForValue().set("k", "v");
        assert Objects.equals(stringRedisTemplate.opsForValue().get("k"), "v");

        // 写入对象，使用JSON序列化
        User user = new User("frankie", 25);
        stringRedisTemplate.opsForValue().set("user", JSONObject.toJSONString(user));
        assert Objects.equals(JSONObject.parseObject(stringRedisTemplate.opsForValue().get("user")).get("userName"), user.getUserName());
    }

}
